
public class Ticket {
	private int[][] numbers;

	public Ticket() {
		numbers = new int[3][9];
		for (int i = 0; i < numbers.length; i++) {
			for (int j = 0; j < numbers[0].length; j++) {
				numbers[i][j] = 0;
			}
		}
		for (int i = 0; i < numbers.length; i++) {
			int cont = 0;
			while (cont < 5) {
				int x = (int) (Math.random() * 9);
				if (validNumber(x)) {
					if (numbers[i][x] == 0) {
						switchMethod(i, x);
						cont++;

					}
				}
			}

		}

	}

	public boolean validNumber(int x) {
		int cont = 0;
		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i][x] != 0) {
				cont++;
			}
		}
		if (cont == 2 ) {
			return false;
		} else {
			return true;
		}

	}

	public void switchMethod(int i, int x) {
		int y;
		switch (x) {
		case 0:
			y = (int) (Math.random() * 9 + 1);
			numbers[i][x] = y;
			break;
		case 1:
			y = (int) (Math.random() * 10 + 10);
			numbers[i][x] = y;
			break;
		case 2:
			y = (int) (Math.random() * 10 + 20);
			numbers[i][x] = y;
			break;
		case 3:
			y = (int) (Math.random() * 10 + 30);
			numbers[i][x] = y;
			break;
		case 4:
			y = (int) (Math.random() * 10 + 40);
			numbers[i][x] = y;
			break;
		case 5:
			y = (int) (Math.random() * 10 + 50);
			numbers[i][x] = y;
			break;
		case 6:
			y = (int) (Math.random() * 10 + 60);
			numbers[i][x] = y;
			break;
		case 7:
			y = (int) (Math.random() * 10 + 70);
			numbers[i][x] = y;
			break;
		case 8:
			y = (int) (Math.random() * 10 + 80);
			numbers[i][x] = y;
			break;
		}
	}

	public String toString() {
		String s = "";
		for (int i = 0; i < numbers.length; i++) {
			for (int j = 0; j < numbers[0].length; j++) {
				if (numbers[i][j] != 0) {
					s += numbers[i][j] +" ";
				} else {
					s += " . ";
				}

			}
			s += "\n";
		}
		return s;
	}

}
